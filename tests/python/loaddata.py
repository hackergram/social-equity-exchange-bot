import mongoengine
import json


import sys
# parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# os.sys.path.insert(0, parentdir)
sys.path.append("../..")

from sex import documents, xpal, seebot

mongoengine.connect('seebot', alias='default')

documents.Member.objects.delete()
documents.Xchange.objects.delete()

seed = open("../testfiles/seed.json", "rb").read()
seed_json = json.loads(seed)
seebot.logger.info(seed_json)

for member in seed_json['members']:
    print(member)
    xpal.create_member(member)

for x in seed_json['xchanges']:
    xpal.create_xchange(x)

# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.
# from seebot.xpal import *
from . import xpal
from . import utils
# from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
#                          ConversationHandler)
from telegram.ext import (CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler, CallbackQueryHandler)
from telegram import ReplyKeyboardMarkup, ParseMode, InlineKeyboardMarkup, InlineKeyboardButton
import xetrapal
from xetrapal import telegramastras


import sys
sys.path.append("/opt/xetrapal")

xchange_open_text = '''\
    <b>{}</b>({})\n
    <b>{}</b> <i>claims - </i>\n
    {}\n
    <i>Payments to be made: </i>\n
    {}\n"
'''
xchange_line_template = '''\
    <b>{}</b>({})\n
    <i>Created By:</i> {}\n
    <i>Involves: </i> {}\n
'''
member_open_text = '''\
    <b>{}</b>({})\n
    {}\n
'''
member_line_template = '''\
    <b>{}</b> - {}\n
'''
main_menu_header_text = '''\
    <b>Hi! Welcome to the हैकरgram Social Equity eXchange</b>.\n
    What would you like to do? Please choose from the options in the keyboard.\n
    आप क्या करना चाहेंगे? कृपया स्क्रीन पर दिये कीबोर्ड में दिये हुए विकल्पों में से चुने \n
'''
list_xchange_text = u'\U0001f4dc List Xchanges'
list_member_text = u'\U0001f9d8 List Members'
add_xchange_text = u'\U00002795 Add Xchange'
open_xchange_text = u'\U0001f4c2 Open Xchange'
open_member_text = u'\U0001f9d8 Open Member'

my_xchange_text = u'\U0001f91d My Xchanges'
add_handoff_text = u'\U0001F91D Handoff'
add_vehicle_text = u'\U0001F695 Vehicle'
send_location_text = u'\U0001F4CD Send Location'
send_contact_text = u'\U0001F4CD Send Contact'
submit_text = u'\U00002714 Submit'
cancel_text = u'\U0000274C Cancel'
start_duty_text = u'\U0001f3c1 Start Duty'
stop_duty_text = u'\U0001f6a9 Stop Duty'
cash_text = u'\U0001f6a9 Cash'
credit_text = u'\U0001f6a9 Credit'

# member_base_keyboard = [[check_in_text, check_out_text], [open_duty_slip_text]]
member_base_keyboard = [
                        [list_xchange_text, add_xchange_text],
                        [list_member_text, my_xchange_text]
                        ]
location_keyboard = [
                        [{'text': send_location_text, 'request_location': True}]
                    ]
contact_keyboard = [
                    [{'text': send_contact_text, 'request_contact': True}]
                    ]
payment_mode_keyboard = [[cash_text], [credit_text]]
# yes_no_keyboard = [[telegram.InlineKeyboardButton("Yes", callback_data='Yes'),telegram.InlineKeyboardButton("No", callback_data='No')]]

memberbotconfig = xetrapal.karma.load_config(configfile="/opt/seebot-appdata/seebot.conf")
seebot = xetrapal.telegramastras.XetrapalTelegramBot(config=memberbotconfig, logger=xpal.seebotxpal.logger)
logger = seebot.logger
GETMOBILE, MENU_CHOICE, XCHANGE_LISTED, XCHANGE_OPEN, MEMBER_LISTED, MEMBER_OPEN, DUTYSLIP_MENU, DUTYSLIP_OPEN, DUTYSLIP_FORM = range(9)


def facts_to_str(user_data):
    facts = list()
    logger.info("Converting facts to string")
    for key, value in user_data.items():
        facts.append(u'{} - {}'.format(key, repr(value)))
    logger.info("Converted facts to string")
    return "\n".join(facts).join(['\n', '\n'])


#def main_menu(bot, update):
def main_menu(update: Update, context: CallbackContext):
    user_data = {}
    try:
        user_data['member'] = xpal.get_member_by_tgid(update.message.from_user.id)
        logger.info(u"{}".format(user_data))
        if user_data['member'] is None:
            update.message.reply_text("This service is for हैकरgram Members participating in the Social Equity eXchange experiment only!")
            markup = ReplyKeyboardMarkup(contact_keyboard)
            update.message.reply_text("If you are logging in for the first time, please share your mobile number", reply_markup=markup)
            # return ConversationHandler.END
            return GETMOBILE
        logger.info("Main Menu presented to member {}".format(user_data['member'].username))
        markup = ReplyKeyboardMarkup(member_base_keyboard, one_time_keyboard=True)
        update.message.reply_photo(photo=open("/var/www/html/sex/sexlogo.png", "rb"))
        update.message.reply_text(main_menu_header_text, reply_markup=markup, parse_mode=ParseMode.HTML)
        return MENU_CHOICE
    except Exception as e:
        logger.info("{} {}".format(type(e), str(e)))


def list_xchanges(bot, update, user_data):
    for xchange in xpal.documents.Xchange.objects():
        keyboard = [
                    [InlineKeyboardButton(open_xchange_text, callback_data=xchange.xchange_id)]
                    ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        memberlist = str(", ".join([member['username'] for member in xchange.members]))
        xchange_text = xchange_line_template.format(xchange.description, xchange.xchange_id, xchange.created_by, memberlist)
        update.message.reply_text(xchange_text, reply_markup=reply_markup, parse_mode=ParseMode.HTML)
    return XCHANGE_LISTED


def open_xchange_button(bot, update, user_data):
    query = update.callback_query
    xchange = xpal.documents.Xchange.objects(xchange_id=query.data)[0]
    logger.info("Opening Xchange {}, {} {}".format(xchange.xchange_id, update, user_data))
    xchange_text = xchange_open_text.format(xchange.description, xchange.xchange_id, xchange.created_by, xchange.members, xchange.repayments)
    keyboard = [
                [InlineKeyboardButton(cancel_text, callback_data=xchange.xchange_id)]
                ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(text=xchange_text, reply_markup=reply_markup, parse_mode=ParseMode.HTML)
    return XCHANGE_OPEN


def close_xchange_button(bot, update, user_data):
    query = update.callback_query
    xchange = xpal.documents.Xchange.objects(xchange_id=query.data)[0]
    memberlist = str(", ".join([member['username'] for member in xchange.members]))
    xchange_text = xchange_line_template.format(xchange.description, xchange.xchange_id, xchange.created_by, memberlist)
    keyboard = [
                [InlineKeyboardButton(open_xchange_text, callback_data=xchange.xchange_id)]
                ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(text=xchange_text, reply_markup=reply_markup, parse_mode=ParseMode.HTML)
    return XCHANGE_LISTED


def add_xchange(bot, update, user_data):
    markup = ReplyKeyboardMarkup(member_base_keyboard, one_time_keyboard=True)
    update.message.reply_text("Still building this!", reply_markup=markup)


def list_members(bot, update, user_data):
    for member in xpal.documents.Member.objects():
        keyboard = [
                    [InlineKeyboardButton(open_member_text, callback_data=member.username)]
                    ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        member_text = member_line_template.format(member.username, member.mobile_num)
        update.message.reply_text(member_text, reply_markup=reply_markup, parse_mode=ParseMode.HTML)
    return MEMBER_LISTED


def open_member_button(bot, update, user_data):
    query = update.callback_query
    member = xpal.documents.Member.objects(username=query.data)[0]
    logger.info("Opening Member {}, {} {}".format(member.username, update, user_data))
    member_text = member_open_text.format(member.username, member.mobile_num, member.repayments)
    keyboard = [
                [InlineKeyboardButton(cancel_text, callback_data=member.username)]
                ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(text=member_text, reply_markup=reply_markup, parse_mode=ParseMode.HTML)
    return MEMBER_OPEN


def close_member_button(bot, update, user_data):
    query = update.callback_query
    member = xpal.documents.Member.objects(username=query.data)[0]
    member_text = member_line_template.format(member.username, member.mobile_num)
    keyboard = [
                [InlineKeyboardButton(open_member_text, callback_data=member.username)]
                ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(text=member_text, reply_markup=reply_markup, parse_mode=ParseMode.HTML)
    return MEMBER_LISTED


def add_member(bot, update, user_data):
    markup = ReplyKeyboardMarkup(member_base_keyboard, one_time_keyboard=True)
    update.message.reply_text("Still building this!", reply_markup=markup)


def set_mobile(bot, update, user_data):
    logger.info(u"{}".format(update.message.contact))
    member = xpal.get_member_by_mobile(update.message.contact.phone_number.lstrip("+"))
    if member:
        member.tgid = update.message.contact.user_id
        member.save()
        user_data['member'] = member
        logger.info("Main Menu presented to member {}".format(user_data['member'].username))
        markup = ReplyKeyboardMarkup(member_base_keyboard, one_time_keyboard=True)
        update.message.reply_photo(photo=open("/var/www/html/sex/sexlogo.png", "rb"))
        update.message.reply_text(main_menu_header_text, reply_markup=markup, parse_mode=ParseMode.HTML)
        return MENU_CHOICE
    else:
        update.message.reply_text("Sorry, you don't seem to be listed!")
        return ConversationHandler.END


def cancel(bot, update, user_data):
    logger.info(u"Cancelling Update {}".format(user_data))
    markup = ReplyKeyboardMarkup(member_base_keyboard, one_time_keyboard=True)
    update.message.reply_text(u'Cancelled!', reply_markup=markup)
    # for key in user_data.keys():
    #    if key != "member":
    #        del user_data[key]
    return MENU_CHOICE


def done(bot, update, user_data):
    if 'choice' in user_data:
        del user_data['choice']
    update.message.reply_text("Bye!")
    user_data.clear()
    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def setup():
    # Create the Updater and pass it your bot's token.
    updater = seebot.updater
    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    # Add conversation handler with the states CHOOSING, TYPING_CHOICE and TYPING_REPLY
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', main_menu)],
        states={
            GETMOBILE: [MessageHandler(Filters.text,
                                       done,
                                       pass_user_data=True),
                        MessageHandler(Filters.contact,
                                       set_mobile,
                                       pass_user_data=True),
                        ],
            MENU_CHOICE: [
                            RegexHandler('^(' + list_xchange_text + ')$', list_xchanges, pass_user_data=True),
                            RegexHandler('^(' + add_xchange_text + ')$', add_xchange, pass_user_data=True),
                            RegexHandler('^(' + list_member_text + ')$', list_members, pass_user_data=True),
                            CallbackQueryHandler(open_xchange_button, pass_user_data=True),
                          ],
            XCHANGE_LISTED: [
                                RegexHandler('^(' + list_xchange_text + ')$', list_xchanges, pass_user_data=True),
                                RegexHandler('^(' + add_xchange_text + ')$', add_xchange, pass_user_data=True),
                                RegexHandler('^(' + list_member_text + ')$', list_members, pass_user_data=True),
                                CallbackQueryHandler(open_xchange_button, pass_user_data=True),
                             ],
            XCHANGE_OPEN: [
                            CallbackQueryHandler(close_xchange_button, pass_user_data=True),
                           ],
            MEMBER_LISTED: [
                                RegexHandler('^(' + list_xchange_text + ')$', list_xchanges, pass_user_data=True),
                                RegexHandler('^(' + add_xchange_text + ')$', add_xchange, pass_user_data=True),
                                RegexHandler('^(' + list_member_text + ')$', list_members, pass_user_data=True),
                                CallbackQueryHandler(open_member_button, pass_user_data=True),
                             ],
            MEMBER_OPEN: [
                            CallbackQueryHandler(close_member_button, pass_user_data=True),
                           ],
        },
        fallbacks=[RegexHandler('^/[dD]one$', done, pass_user_data=True)]
    )
    dp.add_handler(conv_handler)
    # log all errors
    dp.add_error_handler(error)
    # Start the Bot
    # updater.start_polling()
    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    # updater.idle()


def single_update():
    p = seebot.get_latest_updates()
    for update in p:
        seebot.updater.dispatcher.process_update(update)
    return p


if __name__ == '__main__':
    setup()
    seebot.updater.start_polling()
    seebot.updater.idle()
